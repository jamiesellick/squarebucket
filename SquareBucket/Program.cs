﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace SquareBucket
{
    class Program
    {
        private static AccessToken _accessToken;
        private static string oauthKey;

        static void Main(string[] args)
        {
            oauthKey = Environment.GetEnvironmentVariable("BitBucketOauthKey");
            if (string.IsNullOrWhiteSpace(oauthKey))
                throw new ConfigurationErrorsException("Environment variable BitBucketOauthKey must be set");

            var changes = new Dictionary<string, Dictionary<string, Operation>>();
            using (var changesReader = new StreamReader("changes.csv"))
            {
                var names = changesReader.ReadLine()?.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (names == null || names.Length == 0)
                    throw new ConfigurationErrorsException("changes.csv contains no usernames");

                var ordinalMap = new Dictionary<int, string>();
                int ordinal = 1;
                foreach (var name in names)
                {
                    changes.Add(name, new Dictionary<string, Operation>());
                    ordinalMap.Add(ordinal, name);
                    ordinal++;
                }



                string line;
                while (!string.IsNullOrWhiteSpace((line = changesReader.ReadLine())))
                {
                    var cols = line.Split(new[] { "," }, StringSplitOptions.None);
                    var repo = cols[0];

                    for (int i = 1; i < cols.Length; i++)
                    {
                        Operation operation;
                        if (!Enum.TryParse(cols[i], out operation))
                            operation = Operation.Ignore;

                        if (operation != Operation.Add)
                            Console.WriteLine(operation);

                        changes[ordinalMap[i]].Add(repo, operation);
                    }
                }
            }

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(Encoding.UTF8.GetBytes(oauthKey)));
                var tokenResponse = JsonConvert.DeserializeObject<AccessTokenResponse>(client
                    .PostAsync("https://bitbucket.org/site/oauth2/access_token?grant_type=client_credentials",
                        new StringContent("grant_type=client_credentials", Encoding.UTF8,
                            "application/x-www-form-urlencoded"))
                    .Result.Content.ReadAsStringAsync().Result);

                _accessToken = new AccessToken(tokenResponse);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _accessToken.Token);

                foreach (var changeSet in changes)
                {
                    foreach (var change in changeSet.Value)
                    {
                        Uri uri = new Uri(
                            $"https://api.bitbucket.org/2.0/repositories/skipthedishes/{change.Key}/default-reviewers/{changeSet.Key}");

                        HttpRequestMessage request = null;

                        switch (change.Value)
                        {
                            case Operation.Add:
                                request = new HttpRequestMessage(HttpMethod.Put, uri.ToString());
                                break;
                            case Operation.Remove:
                                request = new HttpRequestMessage(HttpMethod.Delete, uri.ToString());
                                break;
                        }

                        if (request != null)
                        {
                            Console.Write($"{changeSet.Key} - {change.Key} - {change.Value}... ");
                            try
                            {
                                var result = client.SendAsync(request).Result;
                                Console.WriteLine(result.IsSuccessStatusCode ? "Success" : $"Failure: {result.StatusCode} - {result.Content.ReadAsStringAsync().Result}");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"Failure: {ex.Message}");
                            }
                        }
                    }
                }

            }

            Console.WriteLine("Done");
            Console.ReadLine();
        }

    }

    class AccessTokenResponse
    {
        public string access_token { get; set; }
        public string scopes { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string token_type { get; set; }
    }

    class AccessToken
    {
        public string Token { get; }
        public string RefreshToken { get; }

        public bool IsExpired => DateTime.Now >= ExpiresAt;

        private DateTime ExpiresAt;

        public AccessToken(AccessTokenResponse response)
        {
            Token = response.access_token;
            RefreshToken = response.refresh_token;
            ExpiresAt = DateTime.Now.AddSeconds(response.expires_in);
        }

    }

    enum Operation
    {
        Add, Remove, Ignore
    }
}
